# Overview

This is a starting of a package to deploy Strimzi Kafka clsuter for ConfigSync clusters.

## Generate

## Update

To update, run the "update.sh" script located in the root of this project


## Apply to Cluster

```yaml
apiVersion: configsync.gke.io/v1beta1
kind: RootSync
metadata:
  name: strimzi-cluster-trait-pkg
  namespace: config-management-system
  annotations:
    configsync.gke.io/deletion-propagation-policy: Foreground
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/mike-ensor/strimzi-cluster-trait-pkg.git"
    branch: "main"
    # revision: "v0.1.0"
    dir: "/config"
    auth: "none"
```

