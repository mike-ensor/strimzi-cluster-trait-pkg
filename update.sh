#!/bin/bash

DOWNLOAD_FOLDER=./base
NAMESPACE=${1:-"strimzi-public"}

echo "Downloading newst version of Stimzi from strimzi.io"
echo "Setting namespace to '${NAMESPACE}'"

# Remove existing file
rm -rf ${DOWNLOAD_FOLDER}/strimzi-latest.yaml

# Replace with latest version
wget https://strimzi.io/install/latest -q -O ${DOWNLOAD_FOLDER}/strimzi-latest.yaml -P ${DOWNLOAD_FOLDER}

# Replace all occurances of 'namespace="myproject" in the file ${DOWNLOAD_FOLDER}/strimzi-latest.yaml
sed -i "s/namespace: myproject/namespace: ${NAMESPACE}/g" ${DOWNLOAD_FOLDER}/strimzi-latest.yaml

echo "Done. Check file in ${DOWNLOAD_FOLDER}/strimzi-latest.yaml"

kustomize build . > "config/strimzi-operator-${NAMESPACE}-latest.yaml"